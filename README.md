<h1>Employment</h1>
<hr/>
A simple web app similar to the search functionality of LinkedIn.
It enables searching and posting job offers and candidates for employment. 
It contains authentication, browsing by various parameters, ranking and paging the results.
<hr/>
This is the client side / frontend portion of the app. 
<hr/>
<h3>Technologies:</h3>
<ul>
    <li>React</li>
</ul>
