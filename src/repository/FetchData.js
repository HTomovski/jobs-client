import {request} from '../utils/RequestBuilder';
//import {forEach} from "react-bootstrap/cjs/utils/ElementChildren";

const BASE_URL = 'http://localhost:8080';

// Job Offers

// export const getJobOffersBySkills = (skillIds, pageNum) => {
//     console.log(BASE_URL + '/job-offers/skills/' + skillIds + "/" + pageNum);
//     return request({
//         url: BASE_URL + '/job-offers/skills/' + skillIds + "/" + pageNum,
//         method: 'GET'
//     });
// };

export const getJobOffersBySkills = (params) => {
    console.log(BASE_URL + '/job-offers/skills?' + params);
    return request({
        url: BASE_URL + '/job-offers/skills?' + params,
        method: 'GET'
    });
};

export const getJobOffersBySkill = (skillId, pageNum) => {
    console.log("fetching jobs from from api");
    return request({
        url: BASE_URL + '/job-offers/skill/' + skillId + "/" + pageNum,
        method: 'GET'
    });
};


export const getJobOffersByExample = (exampleJobOffer, pageNum) => {
    return request({
        url: BASE_URL + '/job-offers/advanced/' + pageNum,
        method: 'POST',
        body: JSON.stringify(exampleJobOffer)
    });
};

export const getJobOffersSeparateProperties = (params) => {
    console.log(BASE_URL + '/job-offers/advanced?' + params);
    return request({
        url: BASE_URL + '/job-offers/advanced?' + params,
        method: 'GET',
    });
}



export const getJobOffersPropertyList = (params) => {
    console.log(BASE_URL + '/job-offers/simple-search?' + params);
    return request({
        url: BASE_URL + '/job-offers/simple-search?' + params,
        method: 'GET',
    });
}

// export const getJobOffersPropertyList = (propertyList, pageNum) => {
//     return request({
//         //url: BASE_URL + '/job-offers/simple-search/' + propertyList + "/" + pageNum,
//         url: BASE_URL + '/job-offers/simple-search',
//         method: 'POST',
//         body: JSON.stringify(
//             [
//                 propertyList,
//                 pageNum
//             ]
//         )
//     });
// }

export const getJobOfferById = (id) => {
    return request({
        url: BASE_URL + '/job-offers/' + id + '/',
        method: 'GET'
    });
};

export const getJobOffersByUser = (userId, pageNum) => {
    return request({
        url: BASE_URL + '/job-offers/employer/' + userId + '/' + pageNum,
        method: 'GET',
    });
}

export const createJobOffer = (jobOffer) => {
    console.log('sending POST request to: ' + BASE_URL + '/job-offers/');
    return request({
        url: BASE_URL + '/job-offers/',
        method: 'PUT',
        body: JSON.stringify(jobOffer)
    });
};


export const updateJobOffer = (jobOffer) => {
    console.log('sending POST request to: ' + BASE_URL + '/job-offers/');
    return request({
        url: BASE_URL + '/job-offers/',
        method: 'PATCH',
        body: JSON.stringify(jobOffer)
    });
};



export const deleteJobOffer = (jobId) => {
    return request({
        url: BASE_URL + '/job-offers/' + jobId,
        method: 'DELETE',
    });
}

// Industries
export const getIndustries = () => {
    return request({
        url: BASE_URL + '/industries/',
        method: 'GET'
    });
};

export const createIndustry = (industryName) => {
    console.log('sending PUT request to: ' + BASE_URL + '/industries/' + industryName);
    return request({
        url: BASE_URL + '/industries/',
        method: 'PUT',
        body: industryName
    });
};


// Skills
export const getSkillsByIndustry = (industryId) => {
    return request({
        url: BASE_URL + '/skills/' + industryId,
        method: 'GET'
    });
};

export const createSkill = (skillName, industryId) => {
    console.log('sending PUT request to: ' + BASE_URL + '/skills/, for skill: ' + skillName + ', industry id: ' + industryId);
    return request({
        url: BASE_URL + '/skills/',
        method: 'PUT',
        body: JSON.stringify({skillName: skillName, industryId: industryId})
    });
};

// Authentication
export const register = (clientInfo, userType) => {
    console.log('sending POST request to: ' + BASE_URL + '/users/register');
    return request({
        url: BASE_URL + '/users/register/' + userType,
        method: 'POST',
        body: JSON.stringify(clientInfo)
    });
};

export const login = (userInfo) => {
    console.log('sending POST request to: ' + BASE_URL + '/users/login');
    return request({
        url: BASE_URL + '/users/login',
        method: 'POST',
        body: JSON.stringify(userInfo)
    });
};

export const whoAmI = (userInfo) => {
    console.log('sending GET request to: ' + BASE_URL + '/users/me');
    return request({
        url: BASE_URL + '/users/me',
        method: 'GET',
        body: JSON.stringify(userInfo)
    });
};

// Users
export const getUserById = (id) => {
    return request({
        url: BASE_URL + '/users/' + id,
        method: 'GET'
    });
};

export const getUsersBySkill = (skillId, pageNum) => {
    // let skillsQueryString = "";
    // for (let i = 0; i < skillsList.length; i++) {
    //     skillsQueryString += skillsList[i] + ",";
    // }
    return request({
        url: BASE_URL + '/users/skill/' + skillId + "/" + pageNum,
        method: 'GET'
    })
};

export const getUsersBySkills = (skillIds) => {
    // let skillsQueryString = "";
    // for (let i = 0; i < skillsList.length; i++) {
    //     skillsQueryString += skillsList[i] + ",";
    // }
    return request({
        url: BASE_URL + '/users/skills/' + skillIds,
        method: 'GET'
    })
};

export const updateUser = (user) => {
    return request({
        url: BASE_URL + '/users/',
        method: 'PATCH',
        body: JSON.stringify(user)
    })
};

export const updateCandidate = (candidate) => {
    return request({
        url: BASE_URL + '/users/candidate',
        method: 'PATCH',
        body: JSON.stringify(candidate)
    })
};

export const updateEmployer = (employer) => {
    return request({
        url: BASE_URL + '/users/employer',
        method: 'PATCH',
        body: JSON.stringify(employer)
    })
};


