//import {getSkillsFromApiByIndustry} from "../repository/FetchData";

export const request = (options) => {
    //console.log("RequestBuilder called for: " + options.url);
    //console.log(options);
    const headers = new Headers({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });

    if (localStorage.getItem("ACCESS_TOKEN")) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem("ACCESS_TOKEN"))
    }


    //URL encoding
    const BASE_URL = "http://localhost:8080/";
    let mapping = options.url.split(BASE_URL)[1];
    //console.log("Mapping: " + mapping);
    let routes = mapping.split("/");


    //options.url = BASE_URL; //vrati go ova za encoding
    //console.log(routes);
    for (let i = 0; i < routes.length; i++) {
        if (routes[i] === "")
            continue;
        //console.log("route part: " + i);
        //console.log(encodeURIComponent(routes[i]));
        //options.url += encodeURIComponent(routes[i]) + "/"; //vrati go ova za encoding
    }


    console.log("Encoded URL: " + options.url);


    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                //console.log("Location AppApi:");
                //console.log(json.valueOf());
                return json;
            })
        ).catch(function (err) {
        console.log(err)
    });
};
