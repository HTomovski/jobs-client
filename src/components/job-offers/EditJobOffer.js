import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../stylesheets/ListItem.css';
import '../../stylesheets/FiltersBar.css';
import '../../stylesheets/Buttons.css';
import '../../stylesheets/FormComponents.css';

import {getIndustries,
    getJobOfferById,
    getSkillsByIndustry,
    updateJobOffer,
    whoAmI} from "../../repository/FetchData";
import {MultiSelect} from "react-multi-select-component";
//import {forEach} from "react-bootstrap/cjs/utils/ElementChildren";
//import '../stylesheets/Login.css'
//import {Link} from "react-router-dom";


class EditJobOffer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            SelectedSkills: [],
            SkillsLoad: 1,
            JobOffer: {},
            Industries: [],
            Skills: [],
            User: [],
            IsLoading: false
        };

        this.loadJob = this.loadJob.bind(this);
        this.loadIndustries = this.loadIndustries.bind(this);
        this.loadSkillsByIndustry = this.loadSkillsByIndustry.bind(this);
        this.setJobIndustry = this.setJobIndustry.bind(this);
        this.showCurrentUserDetails = this.showCurrentUserDetails.bind(this);
        this.saveEdits = this.saveEdits.bind(this);
        this.cancelChanges = this.cancelChanges.bind(this);
    }

    loadJob() {
        // console.log("window.location:");
        // console.log(window.location);
        let jobId = window.location.toString().split("/job-offers/edit/")[1];
        getJobOfferById(jobId).then(res => {
            console.log(res);
            this.setState({
                JobOffer: res,
                IsLoading: false
            });

            if (this.state.JobOffer.requiredSkills.length > 0) {
                this.setJobIndustry();
            }
        });
    }

    componentDidMount() {
        this.loadIndustries();
        this.loadJob();
        this.showCurrentUserDetails();
    }

    loadIndustries() {
        this.setState({
            IsLoading: true
        });

        getIndustries()
            .then(response => {
                this.setState({
                    Industries: response,
                    IsLoading: false
                });

                console.log("Loaded Industries:");
                console.log(this.state.Industries);
            });
    }


    loadSkillsByIndustry(id) {
        this.setState({
            IsLoading: true
        });

        getSkillsByIndustry(id)
            .then(response => {
                if (response === null || response.length === 0)
                    return;
                this.setState({
                    Skills: response,
                    SelectedSkills: this.state.JobOffer.requiredSkills.map(skill => {
                        return {value: skill.id, label: skill.name};
                    }),
                    IsLoading: false
                });
                console.log("Loaded Skills for industryId:" + id);
                console.log(this.state.Skills);
                if (this.state.SkillsLoad) {
                    this.setState({SkillsLoad: 0});
                }

            }).catch();

    }

    industrySelectionChanged() {
        console.log("industry selection changed, and the index is:");
        let select = document.getElementById("selectInd");
        if (select) {
            console.log(select.selectedIndex);
            if (select.selectedIndex > 0) {
                this.loadSkillsByIndustry(select.selectedIndex);
                //this.loadSkillsByIndustry(this.state.Industries[select.selectedIndex]);
            }
        } else {
            console.log("No industry was selected");
        }

    }

    showCurrentUserDetails() {
        whoAmI().then(response => {
            console.log(response);
            if (response !== null && response !== undefined) {
                this.setState({
                    User: response
                })
            }
        });
    }

    setJobIndustry() {
        let ind = document.getElementById("selectInd");
        console.log(ind);
        console.log(this.state.JobOffer.requiredSkills[0]);
        ind.value = this.state.JobOffer.requiredSkills[0].industry.id;
        console.log(this.state.JobOffer.requiredSkills[0].industry.id);
        console.log(ind.value);
        this.industrySelectionChanged();
    }


    saveEdits() {
        let name = document.getElementById("name");
        let location = document.getElementById("location");
        //let industry = document.getElementById("selectInd");
        //let skills = document.getElementsByClassName("skill-item-purple");
        const skills = this.state.SelectedSkills;
        let description = document.getElementById("description");


        let skillIds = [];
        skills.forEach(skill => /*skillIds.push({*/
            console.log(skill)
        );
        for (let i = 0; i < skills.length; i++) {
            //let user = {};
            skillIds.push({
                id: skills[i].value,
                //industry: industry.value
            });
        }


        // let skillIds = [];
        // for (let i = 0; i < skills.length; i++) {
        //     //let user = {};
        //     skillIds.push({
        //         id: skills[i].id,
        //         industry: industry.value
        //     });
        // }
        //console.log(skillIds);
        let jobOffer = {
            id: this.state.JobOffer.id,
            name: name.value,
            location: location.value,
            employer: this.state.User,
            //industry: industry.value,
            requiredSkills: skillIds,
            shortJobDescription: description.value
        };
        console.log(jobOffer);
        if (jobOffer.name === "" || jobOffer.requiredSkills.length === 0) {
            alert("Please enter a name and at least one required skill");
            return;
        }
        updateJobOffer(jobOffer).then(result => {
            console.log(result);
            window.location.replace("http://localhost:3000/job-offers/detailed/" + result.id);
        })
    };

    cancelChanges() {
        window.location.replace("http://localhost:3000/job-offers/detailed/" + this.state.JobOffer.id);
    }

    render() {
        const industriesList = [];

        this.state.Industries.forEach(industry => {
            industriesList.push(<option
                key={industry.id}
                value={industry.id}
            >{industry.name}</option>)
        });

        const skillsMulti = [];
        if (this.state.Skills !== null && this.state.Skills.length !== 0) {
            this.state.Skills.forEach(skill => {
                skillsMulti.push({
                    value: skill.id,
                    label: skill.name
                });

            });
        }


        return (
            // this.name = name;
            // this.location = location;
            // this.employer = employer;
            // this.requiredSkills = requiredSkills;
            // this.additionalSkills = additionalSkills;
            // this.shortJobDescription = shortJobDescription;
            <div className={"container"}>
                <div className="row">
                    <div className="col-md-6 mx-auto">

                        <div className={"main-content"}>
                            <div>
                                <h3>Edit this Job offer</h3>
                            </div>
                            <div className="form-group">
                                <label htmlFor="name">Job Title</label>
                                <input id="name"
                                       type="text"
                                       className="form-control"
                                       name="name"
                                       defaultValue={this.state.JobOffer.name}
                                       placeholder="Enter Job Title"/>
                            </div>


                            <div className="form-group">
                                <label htmlFor="location">Location</label>
                                <input id="location"
                                       type="text"
                                       className="form-control"
                                       name="location"
                                       defaultValue={this.state.JobOffer.location}
                                       placeholder="e.g. Skopje, Macedonia"/>
                            </div>
                            <div className={"form-group"}>
                                <label htmlFor={"selectInd"}>Industry</label>
                                <select id={"selectInd"}
                                        onChange={() => this.industrySelectionChanged()}
                                        className={"form-control"}>
                                    <option key={-1}
                                            value={-1}>Select an industry
                                    </option>
                                    {industriesList}
                                </select>
                            </div>
                            {/*<div className={"form-group"}>*/}
                            <div className={"margin-bottom-20px"}>
                                <label id={"lblSkillsMulti"} htmlFor={"skillsMulti"}>Required skills</label>

                                <MultiSelect
                                    id={"skillsMulti"}
                                    options={skillsMulti}
                                    value={this.state.SelectedSkills}
                                    onChange={selected => this.setState({SelectedSkills: selected},
                                        () => console.log(this.state.SelectedSkills))}
                                    hasSelectAll={false}
                                    labelledBy={"lblSkillsMulti"}/>
                            </div>
                            {/*</div>*/}

                            {/*<div className={"form-group"}>*/}
                            {/*    <div>*/}
                            {/*        <label htmlFor={""}>Additional skills</label>*/}
                            {/*        <div/>*/}

                            {/*        {skillsList}*/}

                            {/*    </div>*/}
                            {/*</div>*/}
                            <div className={"form-group"}>
                                <label htmlFor={"description"}>Short description</label>
                                <textarea id={"description"}
                                          className={"form-control"}
                                          defaultValue={this.state.JobOffer.name}/>
                            </div>
                            <div align={"center"} className={"display-flex"}>
                                <div className={"width40"}>
                                    <input id={"deleteBtn"} type={"submit"}
                                           onClick={() => this.cancelChanges()} value={"Cancel"}
                                           className={'form-submit form-submit-gray edit-button'}/>
                                </div>
                                <div className={"width40"}>
                                    <input id={"saveUpdateBtn"} type={"submit"}
                                           onClick={() => this.saveEdits()} value={"Save"}
                                           className={'form-submit edit-button'}/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default EditJobOffer;