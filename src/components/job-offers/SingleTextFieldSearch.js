import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../stylesheets/Navbar.css';
import '../../stylesheets/Container.css';
import '../../stylesheets/FiltersBar.css';
import '../../stylesheets/ListItem.css';
import {getJobOffersPropertyList, whoAmI,} from "../../repository/FetchData";
import JobCard from "../job-offers/JobCard";
import History from "../../utils/History";

class SingleTextFieldSearch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            PageNum: 0,
            Jobs: [],
            IsLoading: false
        };
    }

    searchFromUrl = () => {
        const params = new URLSearchParams(window.location.search);
        console.log(params.toString());
        const paramWords = params.get("searchParams");

        const page = Number(params.get("page"));

        if (page !== null && paramWords !== null) {
            document.getElementById("inputField").value = paramWords.replaceAll(",", " ");
            this.setState({PageNum: page}, () => this.simpleSearch(true));
        } else {
            document.getElementById("inputField").value = "";
            this.setState({Jobs: []});
        }
    }

    componentDidMount = () => {
        console.log("Component mounted");

        this.searchFromUrl();
        window.addEventListener('popstate', this.searchFromUrl);

        this.linkPageButtons();
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        console.log("Component Updated");

        this.linkPageButtons();
    }

    showCurrentUserDetails = () => {
        whoAmI().then(response => {
            console.log(response);
        });
    }

    simpleSearch = (backButtonClicked = false) => {
        //console.log(document.getElementById("inputField").value);
        const properties = document.getElementById("inputField").value;
        if (properties.length === 0) {
            alert("Please enter a search parameter");
            return;
        }

        const propertiesSeparated = properties.trim().split(" ");

        const params = new URLSearchParams({
            searchParams: propertiesSeparated,
            page: this.state.PageNum
        });

        console.log("Requesting JobOffers by properties qs: " + params);

        if (!backButtonClicked) {
            History.push(window.location.pathname + "?" + params);
            console.log(History);
        }

        getJobOffersPropertyList(params)
            .then(response => {

                this.setState({
                    Jobs: response,
                    IsLoading: false
                })
            })
    }

    linkPageButtons = () => {
        const btnPrev = document.getElementById("btnPrev");
        const btnNext = document.getElementById("btnNext");
        // console.log(btnPrev);
        // console.log(btnNext);
        if (btnPrev === null || btnPrev === undefined || btnNext === null || btnNext === undefined)
            return;

        btnPrev.setAttribute("style", "display:none");
        btnNext.setAttribute("style", "display:none");

        if (this.state.Jobs === undefined || this.state.Jobs.content === undefined || this.state.Jobs.content.length === 0) {
            return;
        }
        // console.log("this.state.Jobs.content.length:");
        // console.log(this.state.Jobs.content.length);
        if (this.state.Jobs.first) {
            console.log("first page");

        } else {
            btnPrev.removeAttribute("style");
            btnPrev.onclick = () => {
                console.log("Clicked prev page button, page = " + this.state.PageNum);
                this.setState((state) => ({
                    PageNum: state.PageNum - 1
                }), this.simpleSearch);
            }
        }
        if (this.state.Jobs.last) {
            console.log("last page");

        } else {
            btnNext.removeAttribute("style");
            // btnNext.onclick = () => {
            //                 this.setState({
            //                 PageNum: this.state.PageNum + 1
            //             })
            btnNext.onclick = () => {
                console.log("Clicked next page button, page = " + this.state.PageNum);
                this.setState((state) => ({
                    PageNum: state.PageNum + 1
                }), this.simpleSearch);
                //this.simpleSearch();
            }
        }
    }

    render() {
        const jobsList = [];
        if (this.state.Jobs !== undefined && this.state.Jobs.length !== 0) {
            this.state.Jobs.content.forEach(job => {
                jobsList.push(<JobCard
                    key={job.id}
                    job={job}/>)
            });
        }

        return (
            <div className={"container"}>
                <div className="main-content">
                    <div className={"form-group"}>
                        <h4>Simple Job offer search</h4>
                    </div>
                    <div className="row">
                        <div className="col-md-9">
                            <input id={"inputField"} type={"text"} className={"form-control"}/>
                        </div>
                        <div className="col-md-3">
                            <input id={"searchBtn"} type={"submit"} className={'btn btn-success btn-block'}
                                   onClick={() => this.setState({PageNum: 0}, this.simpleSearch)} value={"Search"}/>
                        </div>

                    </div>

                    <div>
                        <div className={"row"}>
                            {jobsList}
                        </div>
                        <div className={"row paging-buttons"}>
                            <div className={"col-md-10 col-lg-10"}>
                                <button id={"btnPrev"}
                                        className={"btn btn-info"}>Previous page
                                </button>
                            </div>

                            <div className={"col-md-2 col-lg-2"}>
                                <button id={"btnNext"}
                                    // style={{float: "right"}}
                                        className={"btn btn-info"}>Next page
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SingleTextFieldSearch;