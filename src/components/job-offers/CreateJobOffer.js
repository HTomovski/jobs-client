import React, {Component} from 'react';
import {MultiSelect} from "react-multi-select-component";
import 'bootstrap/dist/css/bootstrap.css';
import '../../stylesheets/ListItem.css';
import '../../stylesheets/FiltersBar.css';
import '../../stylesheets/FormComponents.css';

import {createJobOffer, getIndustries, getSkillsByIndustry, whoAmI} from "../../repository/FetchData";

class CreateJobOffer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            industries: [],
            selectedIndustry: -1,
            selectedSkills: [],
            skills: [],
            user: [],
            isLoading: false,
            title: undefined,
            location: undefined,
            description: undefined
        };
    }

    loadIndustries = () => {
        this.setState({
            isLoading: true
        });

        getIndustries()
            .then(response => {
                this.setState({
                    industries: response,
                    isLoading: false
                });

                console.log("Loaded industries:");
                console.log(this.state.industries);
            });
    }

    loadSkillsByIndustry = (id) => {
        this.setState({
            isLoading: true
        });

        getSkillsByIndustry(id)
            .then(response => {
                if (response === null || response.length === 0)
                    return;
                this.setState({
                    skills: response,
                    isLoading: false
                }, () => console.log("new skills loaded"));
                console.log("Loaded skills for industry Id:" + id);
                console.log(this.state.skills);
            }).catch();
    }

    clearSkills = () => {
        this.setState({skills: []});
    }

    componentDidMount = () => {
        this.loadIndustries();
        this.showCurrentUserDetails();
    }

    industrySelectionChanged = () => {
        console.log("industry selection changed, and the index is:");
        const indId = this.state.selectedIndustry;
        if (indId != null) {
            console.log(indId);
            if (indId > 0) {
                this.loadSkillsByIndustry(indId);
            } else {
                this.clearSkills();
            }
        } else {
            console.log("null");
        }
    }

    showCurrentUserDetails = () => {
        whoAmI().then(response => {
            console.log(response);
            if (response !== null && response !== undefined) {
                this.setState({
                    user: response
                })
            }
        });
    }

    submitNewJobOffer = () => {
        const skills = this.state.selectedSkills;

        console.log("Selected skills:");
        console.log(skills);

        const skillIds = [];
        skills.forEach(skill =>
            console.log(skill)
        );
        for (let i = 0; i < skills.length; i++) {
            skillIds.push({
                id: skills[i].value,
            });
        }
        const jobOffer = {
            name: this.state.title,
            location: this.state.location,
            employer: this.state.user,
            requiredSkills: skillIds,
            shortJobDescription: this.state.description
        };
        console.log(jobOffer);
        if (jobOffer.name === "" || jobOffer.requiredSkills.length === 0) {
            alert("Please enter a name and at least one required skill");
            return;
        }
        createJobOffer(jobOffer).then(result => {
            console.log(result);
            window.location.replace("http://localhost:3000/job-offers/detailed/" + result.id);
        })
    };

    render() {
        console.log("Render happening!");
        const industriesList = [];

        this.state.industries.forEach(industry => {
            industriesList.push(<option
                key={industry.id}
                value={industry.id}
            >{industry.name}</option>);
        });

        const skillsMulti = [];
        if (this.state.skills !== null && this.state.skills.length !== 0) {
            this.state.skills.forEach(skill => {
                skillsMulti.push({
                    value: skill.id,
                    label: skill.name
                });
            });
        }

        return (
            // this.name = name;
            // this.location = location;
            // this.employer = employer;
            // this.requiredSkills = requiredSkills;
            // this.additionalSkills = additionalSkills;
            // this.shortJobDescription = shortJobDescription;
            <div className={"container"}>
                <div className="row">
                    <div className="col-md-6 mx-auto">

                        <div className={"main-content"}>

                            <div>
                                <h3>Create a Job offer</h3>
                            </div>

                            <div className="form-group">
                                <label htmlFor="name">Job Title</label>
                                <input id="name"
                                       onChange={event => this.setState({title: event.target.value})}
                                       type="text"
                                       className="form-control"
                                       name="name"
                                       placeholder="Enter Job Title"/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="location">Location</label>
                                <input id="location"
                                       onChange={event => this.setState({location: event.target.value})}
                                       type="text"
                                       className="form-control"
                                       name="location"
                                       placeholder="e.g. Skopje, Macedonia"/>
                            </div>
                            <div className={"form-group"}>
                                <label htmlFor={"selectInd"}>Industry</label>
                                <select id={"selectInd"}
                                        onChange={event => {
                                            this.setState({selectedIndustry: event.target.value},
                                                () => this.industrySelectionChanged());
                                        }}
                                        className={"form-control"}>
                                    <option key={-1}
                                            value={-1}>Select an industry
                                    </option>
                                    {industriesList}
                                </select>
                            </div>
                            <div className={"margin-bottom-20px"}>
                                <label id={"lblSkillsMulti"} htmlFor={"skillsMulti"}>Required skills</label>

                                <MultiSelect
                                    id={"skillsMulti"}
                                    options={skillsMulti}
                                    value={this.state.selectedSkills}
                                    onChange={selected => this.setState({selectedSkills: selected},
                                        () => console.log(this.state.selectedSkills))}
                                    hasSelectAll={false}
                                    labelledBy={"lblSkillsMulti"}/>
                            </div>

                            <div className={"form-group"}>
                                <label htmlFor={"description"}>Short description</label>
                                <textarea id={"description"}
                                    //onfocusout={}
                                          onChange={event => this.setState({description: event.target.value})}
                                          className={"form-control"}/>
                            </div>
                            <div align={"center"}>
                                <input id={"createBtn"} type={"submit"} className={'form-submit'}
                                       onClick={() => this.submitNewJobOffer()} value={"Create"}/>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default CreateJobOffer;