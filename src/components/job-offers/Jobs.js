import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../stylesheets/Navbar.css'
//import {Link} from "react-router-dom";
import { getJobOffersBySkills } from "../../repository/FetchData";
import JobCard from "./JobCard";

class Jobs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            Jobs: [],
            IsLoading: false
        };

        this.loadJobs = this.loadJobs.bind(this);
    }
    loadJobs() {
        this.setState({
            IsLoading: true
        });

        let url = window.location.href.toString();
        let skillId = url.split('/skill/')[1];

        getJobOffersBySkills(skillId)
            .then(response =>{
                this.setState({
                    Jobs: response,
                    IsLoading: false
                });
            })
    }

    componentDidMount() {
        this.loadJobs();
    }

    render() {
        const jobsList = [];

        this.state.Jobs.forEach(job => {
            jobsList.push(<JobCard
                key = {job.id}
                job = {job}/>)
        });

        let noResultsFound;

        if (jobsList.length === 0) {
            noResultsFound = <h4>No results were found.</h4>
        }
        return (
            <div className={"container"}>
                <div align={"center"}>
                    {noResultsFound}
                    {jobsList}
                </div>
            </div>

        )

    }
}

export default Jobs;