import React, {Component} from "react"
import 'bootstrap/dist/css/bootstrap.css';
import {Link} from "react-router-dom";
import {deleteJobOffer} from "../../repository/FetchData";

class JobCard extends Component {
    constructor(props) {
        super(props);
        this.deleteJobOfferClick = this.deleteJobOfferClick.bind(this);
    }

    deleteJobOfferClick(id) {
        let confirmation = window.confirm("Confirm deleting job offer");
        if (confirmation) {
            // prati do server req so method delete
            deleteJobOffer(id).then(resp =>
                //console.log(resp)

            //todo otkoga ke se sredat tie sto treba da se so querystring, ova treba da odi vo history nekako
                window.location.replace("http://localhost:3000/job-offers/posted-by-me")
            ).catch(rej =>
                prompt("Deletion failed")
            );

        }
    }

    render() {
        //console.log(this.props);
        let primarySkills = [];
        for (let i = 0; i < this.props.job.requiredSkills.length; i++) {
            //console.log(this.props.job.requiredSkills[i].name);
            primarySkills.push(
                <p className="card-text" key={i}>{this.props.job.requiredSkills[i].name}</p>
            );
        }
        let links = [];
        // console.log("localstorage stuffs:");
        // console.log(localStorage.getItem("USER"));
        const userLoggedIn = JSON.parse(localStorage.getItem("USER"));
        links.push(
            <Link to={'/job-offers/detailed/' + this.props.job.id} className="btn btn-primary btn-block" key={0}>Details
                <i className={"fa fa-fw fa-chevron-right"}/></Link>
        );
        // console.log("job.employer.id:")
        // console.log(this.props.job.employer.id);
        // console.log("user from localstorage.id:")
        // console.log(userLoggedIn.id);
        // console.log("user from localstorage[id]:")
        // console.log(userLoggedIn["id"]);
        if (userLoggedIn && this.props.job.employer.id === userLoggedIn.id) {
            //console.log("=== USER IS THE CREATOR OF THIS JOB OFFER ===");
            links.push(
                <Link to={'/job-offers/edit/' + this.props.job.id} className="btn btn-info btn-block" key={1}>Edit <i
                    className={"fa fa-fw fa-chevron-right"}/></Link>,
                <button type={"submit"} onClick={() => this.deleteJobOfferClick(this.props.job.id)} className="btn btn-danger btn-block" key={2} value={"Close"}> Close <i
                    className={"fa fa-fw fa-chevron-right"}/> </button>
            );
        }


        return (
            <div className={"col-md-6 col-lg-4"}>
                <div className="card card_margin">
                    <div className="card-body">
                        <h5 className="card-title">{this.props.job.name}</h5>
                        <p className="card-text">{this.props.job.industry}</p>
                        <p className="card-text">{this.props.job.location}</p>
                        {primarySkills}

                        {links}

                        {/*<Link to={'/job-offers/detailed/' + this.props.job.id} className="btn btn-primary btn-block">Details <i className={"fa fa-fw fa-chevron-right"}/></Link>*/}
                    </div>
                </div>
            </div>

        )

    }
}


export default JobCard;