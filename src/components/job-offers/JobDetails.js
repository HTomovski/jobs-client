import React, {Component} from "react"
import 'bootstrap/dist/css/bootstrap.css';
import {getJobOfferById, deleteJobOffer} from "../../repository/FetchData";
import {Link} from "react-router-dom";
//import {Link} from "react-router-dom";

class JobDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            JobOffer: [],
            IsLoading: false
        };
        this.loadJobOffer = this.loadJobOffer.bind(this);
        this.deleteJobOfferDetailed = this.deleteJobOfferDetailed.bind(this);
    }

    componentDidMount() {
        this.loadJobOffer();
    }

    loadJobOffer() {
        //console.log(window.location.toString());
        //console.log(window.location.toString().split("/profile/view/")[1]);
        this.setState({
            IsLoading: true
        });
        let jobId = window.location.toString().split("/job-offers/detailed/")[1];
        console.log(jobId);
        getJobOfferById(jobId).then(res => {
            this.setState({
                JobOffer: res,
                IsLoading: false
            });
            console.log(res);
        });

    }
    deleteJobOfferDetailed() {
    let confirmation = window.confirm("Confirm deleting job offer");
        if (confirmation) {
            // prati do server req so method delete
            deleteJobOffer(this.state.JobOffer.id).then(resp =>
                //console.log(resp)
                window.location.replace("http://localhost:3000/job-offers/posted-by-me")
            ).catch(rej =>
                prompt("Deletion failed")
            );

        }
    }

    render() {
        let display;
        if (this.state.IsLoading) {
            display = <span>Throbber</span>
        } else {
            const requiredSkills = [];
            const location = {};
            const cv = {};
            const shortDesc = {};
            const employer = {};
            const links = [];
            const userLoggedIn = JSON.parse(localStorage.getItem("USER"));

            if (this.state.JobOffer !== undefined) {

                if (this.state.JobOffer.requiredSkills) {
                    for (let i = 0; i < this.state.JobOffer.requiredSkills.length; i++) {
                        //console.log(this.props.job.requiredSkills[i].name);
                        requiredSkills.push(
                            <p className={"skill-user"} style={{float: "left"}}
                               key={i}>{this.state.JobOffer.requiredSkills[i].name}</p>
                        );
                    }
                }
                if (this.state.JobOffer.location) {
                    location.data = (
                        <div className={"div-margin"}>
                            <h4>Location</h4>
                            {this.state.JobOffer.location}
                        </div>
                    )
                }
                if (this.state.JobOffer.shortJobDescription) {
                    shortDesc.data = (
                        <div className={"div-margin"}>
                            <h4>Short description</h4>
                            {this.state.JobOffer.shortJobDescription}
                        </div>
                    )
                }
                if (this.state.JobOffer.cv) {
                    cv.data = (
                        <div className={"div-margin"}>
                            <h4>CV</h4>
                            {this.state.JobOffer.cv}
                        </div>
                    )
                }

                if (this.state.JobOffer.employer) {
                    employer.data = (
                        <div className={"div-margin"}>
                            <h4>Employer</h4>
                            {this.state.JobOffer.employer.email}
                        </div>
                    );
                }

                    if (this.state.JobOffer.employer && userLoggedIn && this.state.JobOffer.employer.id === userLoggedIn.id) {
                        console.log("=== USER IS THE CREATOR OF THIS JOB OFFER ===");
                        links.push(
                            <Link to={'/job-offers/edit/' + this.state.JobOffer.id} className="btn btn-info btn-block" key={0}>Edit <i
                                className={"fa fa-fw fa-chevron-right"}/></Link>,
                            <button type={"submit"} onClick={() => this.deleteJobOfferDetailed()} className="btn btn-danger btn-block" key={1} value={"Close"}> Close <i
                                className={"fa fa-fw fa-chevron-right"}/> </button>
                        );
                    }

                display =
                    <div>
                        <h2>
                            {this.state.JobOffer.name}
                        </h2>
                        <div className={"div-margin"} style={{paddingBottom: 40 + "px"}}>
                            <h4>Required skills</h4>
                            {requiredSkills}
                        </div>

                        {location.data}
                        {shortDesc.data}
                        {cv.data}
                        {employer.data}
                        {links}
                    </div>
            }
        }
        /*while(this.state.IsLoading){

        }*/
        //console.log(user);
        return (
            <div className={"container"}>
                <div className={"main-content"}>
                    {display}

                </div>
            </div>
        );
    }
}


export default JobDetails;