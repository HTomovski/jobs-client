import React, {Component} from "react";
import JobCard from "./JobCard";
import 'bootstrap/dist/css/bootstrap.css';
import '../../stylesheets/FiltersBar.css';
import '../../stylesheets/ListItem.css';
import {getJobOffersByUser, whoAmI} from "../../repository/FetchData";

class MyJobOffers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            PageNum: 0,
            Jobs: [],
            IsLoading: false
        };

        this.showCurrentUserDetails = this.showCurrentUserDetails.bind(this);
        //this.searchBySkills = this.searchBySkills.bind(this);
        this.linkPageButtons = this.linkPageButtons.bind(this);
        //this.clearSearch = this.clearSearch.bind(this);
        this.loadJobs = this.loadJobs.bind(this);
    }

    componentDidMount() {
        this.showCurrentUserDetails();
        this.loadJobs();
        this.linkPageButtons();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.linkPageButtons();
    }


    loadJobs() {
        const user = JSON.parse(localStorage.getItem("USER"));
        const jobOffer = {
            name: null,
            location: null,
            // employer: this.state.User,
            employer: user.id,
            //industry: industry.value,
            requiredSkills: null,
            shortJobDescription: null
        };
        console.log(jobOffer);
        getJobOffersByUser(user.id, this.state.PageNum)
            .then(response => {
                console.log("Response: ");
                console.log(response);
                this.setState({
                    Jobs: response,
                    IsLoading: false
                })
            })
    }

    linkPageButtons() {
        const btnPrev = document.getElementById("btnPrev");
        const btnNext = document.getElementById("btnNext");
        console.log(btnPrev);
        console.log(btnNext);
        if (btnPrev === null || btnPrev === undefined || btnNext === null || btnNext === undefined)
            return;

        btnPrev.setAttribute("style", "display:none");
        btnNext.setAttribute("style", "display:none");
        console.log("this.state.Jobs.content.length:");

        if (this.state.Jobs === undefined || this.state.Jobs.content === undefined || this.state.Jobs.content.length === 0) {
            return;
        }
        console.log(this.state.Jobs.content.length);
        if (this.state.Jobs.first) {
            console.log("first page");

        } else {
            btnPrev.removeAttribute("style");
            btnPrev.onclick = () => {
                this.setState({
                    PageNum: this.state.PageNum - 1
                },
                    () => getJobOffersByUser(this.state.User.id, this.state.PageNum)
                    .then(response => {
                        console.log("Response: ");
                        console.log(response);
                        this.setState({
                            Jobs: response,
                            IsLoading: false
                        })
                    })
                )
            }
        }
        if (this.state.Jobs.last) {
            console.log("last page");

        } else {
            btnNext.removeAttribute("style");
            btnNext.onclick = () => {
                this.setState({
                    PageNum: this.state.PageNum + 1
                },
                    () => getJobOffersByUser(this.state.User.id, this.state.PageNum)
                    .then(response => {
                        console.log("Response: ");
                        console.log(response);
                        this.setState({
                            Jobs: response,
                            IsLoading: false
                        })
                    })
                )
            }
        }


    }

    showCurrentUserDetails() {
        whoAmI().then(response => {
            console.log(response);
            if (response !== null && response !== undefined) {
                this.setState({
                    User: response
                })
            }
        });
    }

    render() {
        let jobsList = [];
        if (this.state.Jobs !== undefined && this.state.Jobs.length !== 0) {
            this.state.Jobs.content.forEach(job => {
                jobsList.push(<JobCard
                    key={job.id}
                    job={job}/>)
            });
        }
        else {
            jobsList = [];
        }

        return (
            <div className={"container"}>
                <div className="main-content">
                    <div><h3>Job Offers posted by me:</h3></div>
                    <div className={"row"}>
                        {jobsList}
                    </div>
                    <div className={"row paging-buttons"}>
                        <div className={"col-md-10 col-lg-10"}>
                            <button id={"btnPrev"}
                                    className={"btn btn-info"}>Previous page
                            </button>
                        </div>

                        <div className={"col-md-2 col-lg-2"}>
                            <button id={"btnNext"}
                                // style={{float: "right"}}
                                    className={"btn btn-info"}>Next page
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default MyJobOffers;