import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
//import '../stylesheets/Login.css'
//import {Link} from "react-router-dom";
import {createIndustry} from "../../repository/FetchData";
import "../../stylesheets/CreateIndustry.css";



class CreateIndustry extends Component {


    createOrUpdateIndustry(){

        if(document.getElementById('indName') == null)
            return;
        let indName = document.getElementById('indName').value;
        if (indName.length === 0) {
            console.log("Please enter ");
            return;
        }
        console.log(indName);
        createIndustry(indName)
            .then(response =>{
               this.setState({
                   NewIndustry: response
               });
                window.alert("Added " + response.name);
                document.getElementById("indName").value = "";
            });
    }

    render() {
        return (
            <div className={'container'}>
            <div className={'industry'}>
                <h3 className={'text-center'}>Create a new Industry category</h3>

                <div className="form-group">
                    <label htmlFor="indName">Name</label>
                    <input id="indName" type="text" className="form-control" name="indName"
                           placeholder="e.g. Marketing" required/>
                </div>
                <div align={"center"}>
                     <input id={"createBtn"} type={"submit"} className={'btn btn-success'}
                     onClick={()=>this.createOrUpdateIndustry()} value={"Create"}/>
                </div>
            </div>
        </div>
        );
    }
}

export default CreateIndustry;