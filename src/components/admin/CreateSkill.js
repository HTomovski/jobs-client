import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {createSkill, getIndustries} from "../../repository/FetchData";
import '../../stylesheets/Container.css';

class CreateSkill extends Component {
    constructor(props) {
        super(props);
        this.state = {
            industries: [],
            selectedIndustry: -1,
            skillName: undefined,
            newSkill: undefined,
            isLoading: false
        };
    }

    loadIndustries = () => {
        this.setState({
            isLoading: true
        });

        getIndustries()
            .then(response => {
                this.setState({
                    industries: response,
                    isLoading: false
                });
            });
        console.log("Loaded industries:");
        console.log(this.state.industries);
    }

    componentDidMount() {
        this.loadIndustries()
    }

    createSkillHandler = () => {
        console.log("Create skill button clicked, the industry index is:");
        const indId = this.state.selectedIndustry;
        const skillName = this.state.skillName;
        if (indId != null && indId > -1) {
            console.log(indId);
            console.log(skillName);
            if (skillName !== undefined)
                if (indId > 0 && skillName !== '') {
                    console.log("Skill to be created - Name: " + skillName + ", indID: " + indId);
                    createSkill(skillName, indId).then(response => {
                        this.setState({
                            newSkill: response
                        });
                        window.alert("Added " + response.name);
                        // TODO: reset the input for the skill name
                        skillName.value = "";
                    });
                }
        } else {
            console.log("null")
        }
    }

    render() {
        const industriesList = [];

        this.state.industries.forEach(industry => {
            industriesList.push(<option
                key={industry.id}
                value={industry.id}
            >{industry.name}</option>)
        });
        return (
            <div className={'container'}>
                <div className={'main-content'}>
                    <div className={"form"}>
                        <form>

                            <h3 className={"text-center"}>Create a new Skill</h3>

                            {/*NAME INPUT*/}
                            <div className={"form-group"}>
                                <label htmlFor="skillName">Skill name</label>
                                <input id="skillName"
                                       type="text"
                                       onChange={event => this.setState({skillName: event.target.value})}
                                       className="form-control"
                                       name="skillName"
                                       placeholder="e.g. Accounting"
                                       required/>
                            </div>

                            <label htmlFor={"select"}>Industry</label>
                            <div className={"form-group"}>
                                <select id={"select"}
                                        className={"form-control"}
                                        onChange={event => this.setState({selectedIndustry: event.target.value})}>
                                    <option key={-1} value={-1}>Select an industry</option>
                                    {industriesList}
                                </select>
                            </div>

                            <div align={"center"}>
                                <input className={'btn btn-success'}
                                       type={'button'}
                                       value={'Create'}
                                       onClick={() => this.createSkillHandler()}/>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateSkill;