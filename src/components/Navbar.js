import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../stylesheets/Navbar.css'
import {Link} from "react-router-dom";

//import {whoAmI} from "../repository/FetchData";

class Navbar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            UserLoggedIn: false
        }

        this.isUserLoggedIn = this.isUserLoggedIn.bind(this);
        this.logOut = this.logOut.bind(this);
    }

    componentDidMount() {
        this.isUserLoggedIn();
    }

    componentWillUnmount() {
        localStorage.clear();
    }

    isUserLoggedIn() {
        // console.log(localStorage);
        // console.log(Date.now());
        const tokenExp = localStorage.getItem("TOKEN_EXP")
        // console.log(tokenExp)
        if (!tokenExp) return false;
        if (Date.now() > Number(tokenExp)) {
            localStorage.clear();
            return false;
        }
        // console.log(localStorage.getItem("ACCESS_TOKEN"));
        return !!localStorage.getItem("ACCESS_TOKEN");
    }

    logOut() {
        localStorage.clear();
        window.location.replace("/login");
    }

    render() {
        let profileLinks = [];
        if (this.isUserLoggedIn()) {
            console.log("a user is logged in");
            //todo: add the new job offer and view job offers links only if the user is an employer
            const userType = localStorage.getItem("USER_TYPE");
            if (userType === "EMPLOYER") {
                profileLinks.push(
                    <li className="nav-item" key={0}>
                        <Link to={"/job-offers/create"} className="nav-link">New Job Offer</Link>
                    </li>,
                    <li className="nav-item" key={1}>
                        <Link to={"/job-offers/posted-by-me"} className="nav-link">View My Job Offers</Link>
                    </li>,
                    <li className="nav-item" key={2}>
                        <Link to={"/profile/view/me"} className="nav-link">My Profile</Link>
                    </li>,
                    <li className="nav-item" key={3}>
                        <Link to={"/profile/update"} className={"nav-link"}>Edit Profile</Link>
                    </li>,
                    <li className="nav-item" key={4}>
                        <div onClick={this.logOut} className={"nav-link pointer-cursor"}>Log Out</div>
                    </li>
                );

            } else {
                profileLinks.push(
                    <li className="nav-item" key={0}>
                        <Link to={"/profile/view/me"} className="nav-link">My Profile</Link>
                    </li>,
                    <li className="nav-item" key={1}>
                        <Link to={"/profile/update"} className={"nav-link"}>Edit Profile</Link>
                    </li>,
                    <li className="nav-item" key={2}>
                        <div onClick={this.logOut} className={"nav-link pointer-cursor"}>Log Out</div>
                    </li>
                );
            }
        } else {
            console.log("there is no user logged in");

            profileLinks.push(
                <li className="nav-item" key={0}>
                    <Link to={"/login"} className="nav-link">Login</Link>
                </li>,
                <li className="nav-item" key={1}>
                    <Link to={"/register"} className="btn btn-success ml-2">Register</Link>
                </li>
            )
        }

        return (
            <nav className="navbar navbar-expand-sm nav_color navbar-dark justify-content-center">
                <div className="container">
                    {/*<input type={"checkbox"} className={"nav-toggle"}/>*/}
                    <ul id={"main-links"} className="navbar-nav main-links">

                        {/*<Link to={"/job-offers/browse"} className="navbar-brand d-flex align-items-center"><strong> Jobs </strong></Link>*/}
                        <Link to={"/job-offers/simple-browse"}
                              className="navbar-brand d-flex align-items-center"><strong> Search Jobs </strong></Link>
                        <Link to={"/job-offers/advanced-browse"}
                              className="navbar-brand d-flex align-items-center"><strong> Advanced
                            Search </strong></Link>
                        {/*<Link to={"/job-seekers/browse"} className={"navbar-brand d-flex align-items-center"}> People </Link>*/}

                        {/*<Link to={"/admin/skill/create"} className="navbar-brand d-flex align-items-center">Skills</Link>*/}
                        {/*<Link to={"/admin/industry/create"} className="navbar-brand d-flex align-items-center">Industries</Link>*/}
                    </ul>
                    <ul id={"profile"} className="navbar-nav ml-auto profile">

                        {profileLinks}

                    </ul>
                </div>
            </nav>

        )
    }
}

export default Navbar;