import React, {Component} from 'react';
import {Router, Route} from 'react-router-dom';
import History from "../utils/History";
import Navbar from './Navbar';
import Footer from './Footer';
import BrowseJobs from './BrowseJobs';
import Login from './Login';
import Register from './Register';
import Jobs from './job-offers/Jobs';
import JobDetails from './job-offers/JobDetails';
import MyJobOffers from './job-offers/MyJobOffers';
import CreateJobOffer from './job-offers/CreateJobOffer';
import EditJobOffer from './job-offers/EditJobOffer';
import Candidates from './users/Candidates';
import CreateProfile from './users/profile/CreateProfile';
import UpdateProfile from './users/profile/UpdateProfile';
import UserProfile from './users/profile/UserProfile';
import CreateIndustry from './admin/CreateIndustry'
import UpdateIndustry from './admin/UpdateIndustry'
import CreateSkill from './admin/CreateSkill'
import UpdateSkill from './admin/UpdateSkill'
import BrowseCandidates from "./users/BrowseCandidates";
import AdvancedJobSearch from "./AdvancedJobSearch";
import SingleTextFieldSearch from "./job-offers/SingleTextFieldSearch";

import '../stylesheets/Container.css'
import '../stylesheets/App.css';


class App extends Component {

    render() {

        return (
            <Router history={History}>
                <Navbar/>
                <div className={"main-container"}>
                    <Route exact path="/" component={SingleTextFieldSearch}/>
                    <Route exact path="/login" component={Login}/>
                    <Route exact path="/register" component={Register}/>

                    {/*Job offers*/}
                    <Route exact path="/job-offers/browse" component={BrowseJobs}/>
                    <Route exact path="/job-offers/advanced-browse" component={AdvancedJobSearch}/>
                    <Route exact path="/job-offers/simple-browse" component={SingleTextFieldSearch}/>
                    <Route exact path="/job-offers/skill/:id" component={Jobs}/>
                    <Route exact path="/job-offers/detailed/:id" component={JobDetails}/>
                    <Route exact path="/job-offers/create" component={CreateJobOffer}/>
                    <Route exact path="/job-offers/edit/:id" component={EditJobOffer}/>
                    <Route exact path="/job-offers/posted-by-me" component={MyJobOffers}/>

                    {/*Candidates*/}
                    <Route exact path="/job-seekers/browse" component={BrowseCandidates}/>
                    <Route exact path="/job-seekers/skill/:id" component={Candidates}/>
                    <Route exact path="/profile/create" component={CreateProfile}/>
                    <Route exact path="/profile/update" component={UpdateProfile}/>
                    <Route exact path="/profile/view/:id" component={UserProfile}/>

                    {/*Admin*/}
                    <Route exact path="/admin/industry/create" component={CreateIndustry}/>
                    <Route exact path="/admin/industry/update" component={UpdateIndustry}/>
                    <Route exact path="/admin/skill/create" component={CreateSkill}/>
                    <Route exact path="/admin/skill/update" component={UpdateSkill}/>

                </div>
                <Footer/>
            </Router>
        );
    }
}

export default App;
