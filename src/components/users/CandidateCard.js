import React, { Component } from "react"
import 'bootstrap/dist/css/bootstrap.css';
import {Link} from "react-router-dom";

class CandidateCard extends Component{

    render(){
        console.log("Entered CandidateCard");
        console.log(this.props);
        let primarySkills = [];
        for (let i = 0; i < this.props.user.skills.length; i++) {
            //console.log(this.props.job.requiredSkills[i].name);
            primarySkills.push(
                <p className="card-text" key={i}>{this.props.user.skills[i].name}</p>
            );
        }



        return(
            <div className={"col-md-6 col-lg-4"}>
                <div className="card card_margin" style={{width:18+"rem"}}>
                    <div className="card-body">
                        <h5 className="card-title">{this.props.user.name}</h5>
                        <p className="card-text">{this.props.user.industry}</p>
                        <p className="card-text">{this.props.user.location}</p>
                        {primarySkills}



                        <Link to={'/profile/view/' + this.props.user.id} className="btn btn-primary btn-block">Details <i className={"fa fa-fw fa-chevron-right"}/></Link>
                    </div>
                </div>
            </div>

        )

    }
}


export default CandidateCard;