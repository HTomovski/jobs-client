import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../../stylesheets/Container.css'
//import {Link} from 'react-router-dom';
import {getUserById} from "../../../repository/FetchData";

class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            User: [],
            IsLoading: false
        };
        this.loadUser = this.loadUser.bind(this);
    }

    componentDidMount() {
        this.loadUser();
    }

    loadUser() {
        //console.log(window.location.toString());
        //console.log(window.location.toString().split("/profile/view/")[1]);
        this.setState({
            IsLoading: true
        });
        let who = window.location.toString().split("/profile/view/")[1];
        if (who === "me") {

            if (!localStorage.USER)
                return;
            const user = JSON.parse(localStorage.USER);
            console.log("Loaded user from localstorage: ");
            console.log(user);
            getUserById(user.id).then(result => {
                console.log("/profile/view/" + who);
                console.log(result);
                this.setState({
                    User: result,
                    IsLoading: false
                });
            }).catch(reason => {
                alert(reason);
            });

            /*
            whoAmI().then(result => {
                if (result)
                getUserById(result.id).then(res => {
                    this.setState({
                        User: res,
                        IsLoading: false
                    });
                    console.log(res);
                });
            }).catch(reject=>
                console.log(reject)
            );
            */

        } else {
            getUserById(who).then(result => {
                console.log("/profile/view/" + who);
                console.log(result);
                this.setState({
                    User: result,
                    IsLoading: false
                });
            });
        }
    }

    render() {
        let display;
        if (this.state.IsLoading) {
            display = <div className={"container"}><div className="loading"><b>Loading..</b><span>Please wait, we are loading the data.</span></div></div>
        } else {
            const skills = [];
            const hometown = {};
            const email = {};
            const cv = {};
            const shortDesc = {};
            const pastEmp = {};
            const companyInfo = {};

            if (this.state.User !== undefined) {

                if (this.state.User.skills !== undefined) {
                    for (let i = 0; i < this.state.User.skills.length; i++) {
                        //console.log(this.props.job.requiredSkills[i].name);
                        skills.push(
                            <p className={"skill-user"} style={{float: "left"}}
                               key={i}>{this.state.User.skills[i].name}</p>
                        );
                        skills.data = (
                            <div className={"div-margin"} style={{paddingBottom: 40 + "px"}}>
                                <h4>Skills</h4>
                                {skills}
                            </div>
                        );
                    }

                }
                if (this.state.User.homeTown !== null &&
                    this.state.User.homeTown !== undefined &&
                    this.state.User.homeTown !== "") {
                    hometown.data = (
                        <div className={"div-margin"}>
                            <h4>Hometown</h4>
                            {this.state.User.homeTown}
                        </div>
                    );
                }
                if (this.state.User.email !== null &&
                    this.state.User.email !== undefined &&
                    this.state.User.email !== "") {
                    email.data = (
                        <div className={"div-margin"}>
                            <h4>E-mail</h4>
                            {this.state.User.email}
                        </div>
                    );
                }
                if (this.state.User.shortDescription !== null &&
                    this.state.User.shortDescription !== undefined &&
                    this.state.User.shortDescription !== "") {
                    shortDesc.data = (
                        <div className={"div-margin"}>
                            <h4>Short description</h4>
                            {this.state.User.shortDescription}
                        </div>
                    );
                }
                if (this.state.User.cv !== null &&
                    this.state.User.cv !== undefined &&
                    this.state.User.cv !== "") {
                    cv.data = (
                        <div className={"div-margin"}>
                            <h4>CV</h4>
                            {this.state.User.cv}
                        </div>
                    );
                }

                if (this.state.User.pastEmployments !== null &&
                    this.state.User.pastEmployments !== undefined &&
                    this.state.User.pastEmployments !== "") {
                    pastEmp.data = (
                        <div className={"div-margin"}>
                            <h4>Past employments</h4>
                            {this.state.User.pastEmployments}
                        </div>
                    );
                }
                if (this.state.User.companyInfo !== null &&
                    this.state.User.companyInfo !== undefined &&
                    this.state.User.companyInfo !== "") {
                    companyInfo.data = (
                        <div className={"div-margin"}>
                            <h4>About the Company</h4>
                            {this.state.User.companyInfo}
                        </div>
                    );
                }

                display =
                    <div>
                        <h2>
                            {this.state.User.name}
                        </h2>
                        {skills.data}
                        {email.data}
                        {hometown.data}
                        {shortDesc.data}
                        {cv.data}
                        {pastEmp.data}
                        {companyInfo.data}

                    </div>
            }
        }
        /*while(this.state.IsLoading){

        }*/
        //console.log(user);
        return (
            <div className={"container"}>
                <div className={"main-content"}>
                    {display}


                </div>
            </div>
        );
    }
}

export default UserProfile;