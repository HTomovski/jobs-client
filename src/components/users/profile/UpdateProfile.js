import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../../stylesheets/FormComponents.css';
import {
    getIndustries,
    getSkillsByIndustry,
    getUserById,
    updateCandidate,
    updateEmployer
} from "../../../repository/FetchData";
import {MultiSelect} from "react-multi-select-component";
//import '../stylesheets/Login.css'
//import {Link} from "react-router-dom";

class UpdateProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            SkillsLoad: 1,
            Industries: [],
            Skills: [],
            SelectedSkills: [],
            User: [],
            UserType: undefined,
            IsLoading: false
        };
    }

    componentDidMount = () => {
        // const first = () => {
        //     this.loadIndustries();
        // }
        // const second = async () => {
        //     await first();
        //     this.showCurrentUserDetails();
        // };
        // first();
        // second().then(res=>console.log(res + " second function finished"));
        // (async () => this.loadIndustries())().then(res => this.showCurrentUserDetails())
        //this.loadIndustries();
        this.showCurrentUserDetails();
    }

    showCurrentUserDetails = () => {
        const userPrincipal = JSON.parse(localStorage.getItem("USER"));
        console.log(userPrincipal);
        if (userPrincipal !== undefined && userPrincipal !== null) {
            getUserById(userPrincipal.id).then(result => {
                console.log(result);
                this.setState({
                    User: result,
                    IsLoading: false,
                    UserType: (result.hasOwnProperty("skills") ? "CANDIDATE" : "EMPLOYER")
                }, () => {
                    console.log(this.state.User);
                    console.log(this.state.UserType);
                    if (this.state.UserType === "CANDIDATE")
                        this.setUserIndustry();
                });
            });

        } else {
            window.location.replace("http://localhost:3000/login");
        }


    }

    loadIndustries = () => {
        this.setState({
            IsLoading: true
        });

        return new Promise(resolve => {
            getIndustries()
                .then(response => {
                    this.setState({
                            Industries: response,
                            IsLoading: false
                        },
                        () => {
                            console.log("Loaded Industries:");
                            console.log(this.state.Industries);
                            resolve("loaded industries");
                        });
                });
            }
        )
    }


    loadSkillsByIndustry = (id) => {
        this.setState({
            IsLoading: true
        });

        getSkillsByIndustry(id)
            .then(response => {
                if (response === null || response.length === 0)
                    return;
                this.setState({
                    Skills: response,
                    SelectedSkills: this.state.User.skills.map(skill => {
                        return {value: skill.id, label: skill.name};
                    }),
                    IsLoading: false
                });
                console.log("Loaded Skills for industryId:" + id);
                console.log(this.state.Skills);
                if (this.state.SkillsLoad) {
                    this.setState({SkillsLoad: 0});
                }

            }).catch();

    }


    industrySelectionChanged = () => {
        console.log("industry selection changed, and the index is:");
        let select = document.getElementById("selectInd");
        if (select != null) {
            console.log(select.selectedIndex);
            if (select.selectedIndex > 0) {
                this.loadSkillsByIndustry(select.selectedIndex);
                //this.loadSkillsByIndustry(this.state.Industries[select.selectedIndex]);
            }
        } else {
            console.log("null")
        }
    }

    setUserIndustry = async () => {
        await this.loadIndustries().then(resolve => console.log("Await call to loadIndustries finished: " + resolve));
        const ind = document.getElementById("selectInd");
        console.log(ind);
        console.log(this.state.User.skills[0]);
        console.log(!this.state.User.skills);
        console.log(!this.state.User.skills[0]);
        //console.log("industry of first skill: " + this.state.User.skills[0].industry.id);
        if (!this.state.User.skills || !this.state.User.skills[0])
            ind.value = -1;
        else
            ind.value = this.state.User.skills[0].industry.id;
        this.industrySelectionChanged();
    }


    updateUser = () => {
        const isCandidate = this.state.UserType === "CANDIDATE"
        const name = document.getElementById("name");
        const location = document.getElementById("location");
        //const email = document.getElementById("email");
        const email = this.state.User.email;
        const description = document.getElementById("description");


        //console.log(skillIds);
        const user = {
            name: name.value,
            homeTown: location.value,
            email: email,
            shortDescription: description.value,
        };
        if (isCandidate) {
            // const industry = document.getElementById("selectInd");
            const skills = this.state.SelectedSkills;
            const cv = document.getElementById("cv");
            const pastEmps = document.getElementById("pastEmps");
            const skillIds = [];
            //console.log(skills);
            for (let i = 0; i < skills.length; i++) {
                //let user = {};
                skillIds.push({
                    id: skills[i].value,
                    //industry: industry.value
                });
            }
            user.skills = skillIds;
            user.cv = cv.value;
            user.pastEmployments = pastEmps.value;
            console.log("Sending patch request for user:");
            console.log(user);
            updateCandidate(user).then(result => {
                window.location.replace("http://localhost:3000/profile/view/" + result.id);
            })
        } else {
            user.companyInfo = document.getElementById("companyInfo").value;
            console.log("Sending patch request for user:");
            console.log(user);
            updateEmployer(user).then(result => {
                window.location.replace("http://localhost:3000/profile/view/" + result.id);
            })
        }
        // //console.log("Original?");
        // //console.log(user);
        // updateUser(user).then(result => {
        //     //console.log("Updated");
        //     //console.log(result);
        //     window.location.replace("http://localhost:3000/profile/view/" + result.id);
        // })
    }

    cancelChanges = () => {
        window.location.replace("http://localhost:3000/profile/view/" + this.state.User.id);
    }

    render() {
        const userType = this.state.UserType;
        const displayFields = [];

        if (userType === "CANDIDATE") {
            const industriesList = [];
            this.state.Industries.forEach(industry => {
                industriesList.push(<option
                    key={industry.id}
                    value={industry.id}
                >{industry.name}</option>)
            });

            const skillsMulti = [];
            if (this.state.Skills !== null && this.state.Skills.length !== 0) {
                this.state.Skills.forEach(skill => {
                    skillsMulti.push({
                        value: skill.id,
                        label: skill.name
                    });

                });
            }
            displayFields.push(
                <div className={"form-group"} key={1}>
                    <label htmlFor={"selectInd"}>Industry</label>
                    <select id={"selectInd"}
                            onChange={() => this.industrySelectionChanged()}
                            className={"form-control"}>
                        <option key={-1}
                                value={-1}>Select an industry
                        </option>
                        {industriesList}
                    </select>
                </div>,
                <div className={"margin-bottom-20px"} key={2}>
                    <label id={"lblSkillsMulti"} htmlFor={"skillsMulti"}>Skills</label>

                    <MultiSelect
                        id={"skillsMulti"}
                        options={skillsMulti}
                        value={this.state.SelectedSkills}
                        onChange={selected => this.setState({SelectedSkills: selected},
                            () => console.log(this.state.SelectedSkills))}
                        hasSelectAll={false}
                        labelledBy={"lblSkillsMulti"}/>
                </div>,
                <div className={"form-group"} key={3}>
                    <label htmlFor={"description"}>Short description</label>
                    <textarea id={"description"}
                              className={"form-control"}
                              defaultValue={this.state.User.shortDescription}
                    />
                </div>,
                <div className={"form-group"} key={4}>
                    <label htmlFor={"cv"}>CV</label>
                    <textarea id={"cv"}
                              className={"form-control"}
                              defaultValue={this.state.User.cv}
                    />
                </div>,
                <div className={"form-group"} key={5}>
                    <label htmlFor={"pastEmps"}>Past employments</label>
                    <textarea id={"pastEmps"}
                              className={"form-control"}
                              defaultValue={this.state.User.pastEmployments}
                    />
                </div>
            );
        } else if (userType === "EMPLOYER") {
            displayFields.push(
                <div className={"form-group"} key={1}>
                    <label htmlFor={"description"}>Short description</label>
                    <textarea id={"description"}
                              className={"form-control"}
                              defaultValue={this.state.User.shortDescription}
                    />
                </div>,
                <div className={"form-group"} key={2}>
                    <label htmlFor={"companyInfo"}>About the Company</label>
                    <textarea id={"companyInfo"}
                              className={"form-control"}
                              defaultValue={this.state.User.companyInfo}
                    />
                </div>
            );
        }


        return (
            <div className={"container"}>
                <div className="row">
                    <div className="col-md-6 mx-auto">
                        <div className={"main-content"}>
                            <div>
                                <h3>Edit profile</h3>
                            </div>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input id="name"
                                       type="text"
                                       className="form-control"
                                       name="name"
                                    //placeholder="Enter Job Title"
                                       defaultValue={this.state.User.name}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="location">Hometown</label>
                                <input id="location"
                                       type="text"
                                       className="form-control"
                                       name="location"
                                       defaultValue={this.state.User.homeTown}/>
                            </div>

                            {displayFields}

                            <div align={"center"} className={"display-flex"}>
                                <div className={"width40"}>
                                    <input id={"deleteBtn"} type={"submit"}
                                           onClick={() => this.cancelChanges()} value={"Cancel"}
                                           className={'form-submit form-submit-gray edit-button'}/>
                                </div>
                                <div className={"width40"}>
                                    <input id={"saveUpdateBtn"} type={"submit"}
                                           onClick={() => this.updateUser()} value={"Save"}
                                           className={'form-submit edit-button'}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UpdateProfile;