import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import CandidateCard from "./CandidateCard";
import {getUsersBySkills} from "../../repository/FetchData";
//import '../stylesheets/Login.css'
//import {Link} from "react-router-dom";

class Candidates extends Component {
    constructor(props) {
        super(props);

        this.state = {
            Users: [],
            IsLoading: false
        };

        this.loadUsers = this.loadUsers.bind(this);
    }

    loadUsers() {
        this.setState({
            IsLoading: true
        });

        let url = window.location.href.toString();
        let skillIds = url.split('/skill/')[1];

        getUsersBySkills(skillIds)
            .then(response => {
                this.setState({
                    Users: response,
                    IsLoading: false
                });
            })
    }

    componentDidMount() {
        this.loadUsers();
    }

    render() {
        const usersList = [];

        this.state.Users.forEach(job => {
            usersList.push(<CandidateCard
                key={job.id}
                job={job}/>)
        });

        let noResultsFound;

        if (usersList.length === 0) {
            noResultsFound = <h4>No results were found.</h4>
        }
        return (
            <div className={"container"}>
                <div align={"center"}>
                    {noResultsFound}
                    {usersList}
                </div>
            </div>

        )

    }
}
export default Candidates;