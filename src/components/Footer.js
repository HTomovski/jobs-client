import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../stylesheets/Footer.css'
import {Link} from "react-router-dom";

class Footer extends Component {


    render() {
        const adminCommands = {};
        let user = JSON.parse(localStorage.getItem("USER"));

        if (user === null || user === undefined || user.authorities[0].authority !== "ROLE_ADMIN") {
            console.log(user);
            adminCommands.data = "";
        }
        else {
            adminCommands.data =
                <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4 ">


                <h6 className="text-uppercase font-weight-bold">Admin</h6>
                <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style={{width: 60+"px"}}/>
                {/*<p>*/}

                <Link to={"/admin/skill/create"} className="d-flex align-items-center">Skills</Link>
                <Link to={"/admin/industry/create"} className="d-flex align-items-center">Industries</Link>
                {/*</p>*/}
                {/*<p>*/}
                {/*    <a href="#">Become an Affiliate</a>*/}
                {/*</p>*/}
                {/*<p>*/}
                {/*    <a href="#">Shipping Rates</a>*/}
                {/*</p>*/}
                {/*<p>*/}
                {/*    <a href="#">Help</a>*/}
                {/*</p>*/}

            </div>
        }

        return (
            <footer className="page-footer font-small unique-color-dark text-color-white">

                <div style={{backgroundColor: "#6351ce"}}>
                    <div className="container">
                        <div className="row py-4 d-flex align-items-center">
                            <div className="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                                <h6 className="mb-0">The quickest way for employers and job seekers to meet.</h6>
                            </div>
                            <div className="col-md-6 col-lg-7 text-center text-md-right">
                                {/*<a className="fb-ic">*/}
                                {/*    <i className="fab fa-facebook-f white-text mr-4"> </i>*/}
                                {/*</a>*/}

                                {/*<a className="tw-ic">*/}
                                {/*    <i className="fab fa-twitter white-text mr-4"> </i>*/}
                                {/*</a>*/}

                                {/*<a className="gplus-ic">*/}
                                {/*    <i className="fab fa-google-plus-g white-text mr-4"> </i>*/}
                                {/*</a>*/}

                                {/*<a className="li-ic">*/}
                                {/*    <i className="fab fa-linkedin-in white-text mr-4"> </i>*/}
                                {/*</a>*/}

                                {/*<a className="ins-ic">*/}
                                {/*    <i className="fab fa-instagram white-text"> </i>*/}
                                {/*</a>*/}

                            </div>


                        </div>


                    </div>
                </div>


                <div className="container text-center text-md-left mt-5">


                    <div className="row mt-3">


                        <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">


                            <h6 className="text-uppercase font-weight-bold">Company name</h6>
                            <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto"
                                style={{width: 60 + "px"}}/>
                            <p>Placeholder</p>

                        </div>


                        {/*<div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">*/}
                        {/*    <h6 className="text-uppercase font-weight-bold">Products</h6>*/}
                        {/*    <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto"*/}
                        {/*        style={{width: 60 + "px"}}/>*/}
                        {/*    /!*<p>*!/*/}
                        {/*    /!*    <a href="#">MDBootstrap</a>*!/*/}
                        {/*    /!*</p>*!/*/}
                        {/*    /!*<p>*!/*/}
                        {/*    /!*    <a href="#">MDWordPress</a>*!/*/}
                        {/*    /!*</p>*!/*/}
                        {/*    /!*<p>*!/*/}
                        {/*    /!*    <a href="#">BrandFlow</a>*!/*/}
                        {/*    /!*</p>*!/*/}
                        {/*    /!*<p>*!/*/}
                        {/*    /!*    <a href="#">Bootstrap Angular</a>*!/*/}
                        {/*    /!*</p>*!/*/}
                        {/*</div>*/}


                        {adminCommands.data}


                        <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">


                            <h6 className="text-uppercase font-weight-bold">Contact</h6>
                            <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto"
                                style={{width: 60 + "px"}}/>
                            <p>
                                <i className="fas fa-home mr-3"/> New York, NY 10012, US</p>
                            <p>
                                <i className="fas fa-envelope mr-3"/> info@example.com</p>
                            <p>
                                <i className="fas fa-phone mr-3"/> + 01 234 567 88</p>
                            <p>
                                <i className="fas fa-print mr-3"/> + 01 234 567 89</p>

                        </div>


                    </div>


                </div>


                <div className="footer-copyright text-center py-3">© 2018 Copyright:
                    <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
                </div>

            </footer>
        )
    }
}

export default Footer;