import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../stylesheets/ListItem.css';
import '../stylesheets/FiltersBar.css';
import '../stylesheets/Buttons.css';

import {getIndustries, getJobOffersSeparateProperties, getSkillsByIndustry} from "../repository/FetchData";
import JobCard from "./job-offers/JobCard";
import History from "../utils/History";
import {MultiSelect} from "react-multi-select-component";

/*
 * todo: make space to display the results,
 *  Implement history and input field pre fill if applicable
 *
 */

class AdvancedJobSearch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pageNum: 0,
            industries: [],
            selectedIndustry: -1,
            skills: [],
            selectedSkills: [],
            jobs: [],
            user: [],
            isLoading: false
        };
    }

    loadIndustries = () => {
        this.setState({
            isLoading: true
        });

        getIndustries()
            .then(response => {
                this.setState({
                    industries: response,
                    isLoading: false
                });

                console.log("Loaded industries:");
                console.log(this.state.industries);
            });
    }


    loadSkillsByIndustry = (id) => {
        this.setState({
            isLoading: true
        });

        getSkillsByIndustry(id)
            .then(response => {
                if (response === null || response.length === 0)
                    return;
                this.setState({
                    skills: response,
                    isLoading: false
                });
                console.log("Loaded skills for industryId:" + id);
                console.log(this.state.skills);
            }).catch();

    }


    searchFromUrl = (event) => {
        // const params = new URLSearchParams(window.location.search);
        // console.log(params.toString());
        // const name = params.get("name");
        // name ? document.getElementById("name").value = name
        //     : document.getElementById("name").value = "";
        //
        // const location = params.get("location");
        // location ? document.getElementById("location").value = location
        //     : document.getElementById("location").value = "";
        //
        // const empName = params.get("empName");
        // empName ? document.getElementById("empName").value = empName
        //     : document.getElementById("empName").value = "";
        //
        // const industry = Number(params.get("selectInd"));
        // const industryInput = document.getElementById("selectInd");
        // industry ? industryInput.value = industry : industryInput.value = -1;
        //
        // const desc = params.get("desc");
        // const descInput = document.getElementById("desc");
        // desc ? descInput.value = desc : descInput.value = "";
        //
        // const page = Number(params.get("page"));
        //
        // page !== null ? this.setState({pageNum: page}, () => this.search(true))
        //     : this.setState({jobs: [], pageNum: 0});
    }

    componentDidMount = () => {
        console.log("Component mounted");

        this.searchFromUrl();
        window.addEventListener('popstate', this.searchFromUrl);


        this.loadIndustries();
        this.linkPageButtons();
    }


    componentDidUpdate = (prevProps, prevState, snapshot) => {
        this.linkPageButtons();
    }

    clearSkills = () => {
        this.setState({skills: []});
    }

    industrySelectionChanged = () => {
        console.log("industry selection changed, and the index is:");
        const indId = this.state.selectedIndustry;
        if (indId != null) {
            console.log(indId);
            if (indId > 0) {
                this.loadSkillsByIndustry(indId);
            } else {
                this.clearSkills();
            }
        } else {
            console.log("null")
        }
    }

    search = (backButtonClicked = false) => {
        // if (searchButton) {
        //     this.setState({pageNum: 0});
        //     console.log("Search called from search button, state.pageNum: " + this.state.pageNum);
        // }
        let name = document.getElementById("name").value;
        let location = document.getElementById("location").value;
        let empName = document.getElementById("empName").value;
        //let industry = document.getElementById("selectInd").value;
        //let skillElements = document.getElementsByClassName("skill-item-purple");
        const skills = this.state.selectedSkills;
        let desc = document.getElementById("desc").value;
        let atLeastOneFieldIsPresent = false;

        //let skillIds = [];
        //console.log(typeof skillElements);
        let skillIds = [];
        skills.forEach(skill => /*skillIds.push({*/
            console.log(skill)
        );
        for (let i = 0; i < skills.length; i++) {
            //let user = {};
            skillIds.push(
                skills[i].value
                //industry: industry.value
            );
        }
        if (name !== "" || location !== "" || skills.length > 0 || desc !== "" ||
            empName !== "" || skillIds > 0)
            atLeastOneFieldIsPresent = true;
        //console.log(skillIds);
        // let jobOffer = {
        //     name: name === "" ? null : name,
        //     location: location === "" ? null : location,
        //     employer: null,
        //     //industry: industry.value,
        //     requiredSkills: skillIds.length === 0 ? null : skillIds,
        //     shortJobDescription: desc === "" ? null : desc
        // };
        // console.log(jobOffer);
        // console.log(skills);
        if (!atLeastOneFieldIsPresent) {
            alert("Please enter at least one search parameter.");
            return;
        }
        const params = new URLSearchParams();
        // const params = new URLSearchParams({
        //     name,
        //     location,
        //     empName,
        //     desc,
        //     skills
        // });

        console.log("SKILL IDS:")
        console.log(skillIds);
        console.log(skillIds.join());

        if (name) params.set("name", name);
        if (location) params.set("location", location);
        if (empName) params.set("empName", empName);
        if (desc) params.set("desc", desc);
        if (skills.length) params.set("skills", skillIds.join(","));
        params.set("page", this.state.pageNum);

        if (!backButtonClicked) {
            History.push(window.location.pathname + "?" + params);
            console.log(History);
        }

        getJobOffersSeparateProperties(params).then(response => {
            console.log("Response: ");
            console.log(response);
            this.setState({
                jobs: response,
                isLoading: false
            })
        }).catch(reason => console.log("Search failed: " + reason));


        // getJobOffersByExample(jobOffer, this.state.pageNum)
        //     .then(response => {
        //         console.log("Response: ");
        //         console.log(response);
        //         this.setState({
        //             jobs: response,
        //             isLoading: false
        //         })
        //     });


        /*
        * searchBySkill(pageNum) {
        if (document.getElementsByClassName("skill-item-purple").length !== 0)
            getJobOffersBySkill(document.getElementsByClassName("skill-item-purple")[0].id, pageNum)
                .then(response => {
                    console.log(response);
                    this.setState({
                        Candidates: response,
                        isLoading: false
                    })
                })
    }
        * */
    };

    linkPageButtons = () => {
        const btnPrev = document.getElementById("btnPrev");
        const btnNext = document.getElementById("btnNext");
        // console.log(btnPrev);
        // console.log(btnNext);
        if (btnPrev === null || btnPrev === undefined || btnNext === null || btnNext === undefined)
            return;

        btnPrev.setAttribute("style", "display:none");
        btnNext.setAttribute("style", "display:none");
        console.log("this.state.jobs.content.length:");

        if (this.state.jobs === undefined || this.state.jobs.content === undefined || this.state.jobs.content.length === 0) {
            return;
        }
        console.log(this.state.jobs.content.length);
        if (this.state.jobs.first) {
            console.log("first page");

        } else {
            btnPrev.removeAttribute("style");
            btnPrev.onclick = () => {
                this.setState({
                    pageNum: this.state.pageNum - 1
                })
                this.search();
            }
        }
        if (this.state.jobs.last) {
            console.log("last page");

        } else {
            btnNext.removeAttribute("style");
            btnNext.onclick = () => {
                this.setState({
                    pageNum: this.state.pageNum + 1
                })
                this.search();
            }
        }
    }

    render() {
        const industriesList = [];

        this.state.industries.forEach(industry => {
            industriesList.push(<option
                key={industry.id}
                value={industry.id}
            >{industry.name}</option>)
        });

        const skillsMulti = [];
        if (this.state.skills !== null && this.state.skills.length !== 0) {
            this.state.skills.forEach(skill => {
                skillsMulti.push({
                    value: skill.id,
                    label: skill.name
                });
            });
        }
        console.log("this.state.jobs:");
        console.log(this.state.jobs);
        const jobsList = [];
        if (this.state.jobs !== undefined && this.state.jobs.length !== 0) {
            this.state.jobs.content.forEach(job => {
                jobsList.push(<JobCard
                    key={job.id}
                    job={job}/>)
            });
        }

        return (
            <div className="main-container">
                <div className="col-md-6 mx-auto">

                    <div className={"main-content"}>
                        <div className={"margin-bottom-20px"}>
                            <h3>Advanced Job offer search</h3>
                        </div>
                        <div className={"margin-bottom-20px row"}>
                            <div className="form-group col-sm-4">
                                <label htmlFor="name">Search by job Title</label>
                                <input id="name"
                                    // onChange={() => this.setState({pageNum: 0})}
                                       type="text"
                                       className="form-control"
                                       name="name"
                                       placeholder="Enter Job Title"/>
                            </div>

                            <div className="form-group col-sm-4">
                                <label htmlFor="location">Search by location</label>
                                <input id="location"
                                    // onChange={() => this.setState({pageNum: 0})}
                                       type="text"
                                       className="form-control"
                                       name="location"
                                       placeholder="e.g. Skopje, Macedonia"
                                />
                            </div>

                            <div className="form-group col-sm-4">
                                <label htmlFor="employer">Search by employer</label>
                                <input id="empName"
                                    // onChange={() => this.setState({pageNum: 0})}
                                       type="text"
                                       className="form-control"
                                       name="employer"
                                       placeholder="Enter company name"
                                />
                            </div>

                            <div className={"form-group col-sm-4"}>
                                <label htmlFor={"selectInd"}>Industry</label>
                                <select id={"selectInd"}
                                        onChange={event => {
                                            this.setState({selectedIndustry: event.target.value},
                                                () => this.industrySelectionChanged());
                                        }}
                                        className={"form-control"}>
                                    <option key={-1}
                                            value={-1}>Select an industry
                                    </option>
                                    {industriesList}
                                </select>
                            </div>
                            <div className={"margin-bottom-20px col-sm-8"}>
                                <label id={"lblSkillsMulti"} htmlFor={"skillsMulti"}>Search in required skills</label>

                                <MultiSelect
                                    id={"skillsMulti"}
                                    options={skillsMulti}
                                    // options={this.state.skills.map(skill => {
                                    //         return {
                                    //             value: skill.id,
                                    //             label: skill.name
                                    //         }
                                    //     }
                                    // )}
                                    value={this.state.selectedSkills}
                                    onChange={selected => this.setState({selectedSkills: selected},
                                        () => console.log(this.state.selectedSkills))}
                                    hasSelectAll={false}
                                    labelledBy={"lblSkillsMulti"}/>
                            </div>

                            <div className={"form-group col-sm-12"}>
                                <label htmlFor={"desc"}>Search in description</label>
                                <textarea id={"desc"} className={"form-control"}/>
                            </div>
                        </div>
                        <div align={"center"}>
                            <input id={"createBtn"} type={"submit"} className={'form-submit'}
                                   onClick={() => this.setState({pageNum: 0}, this.search)} value={"Search"}/>
                        </div>
                        <div className={"row"}>
                            {jobsList}
                        </div>

                        <div className={"row paging-buttons"}>
                            <div className={"col-md-10 col-lg-10"}>
                                <button id={"btnPrev"}
                                        className={"btn btn-info"}>Previous page
                                </button>
                            </div>

                            <div className={"col-md-2 col-lg-2"}>
                                <button id={"btnNext"}
                                    //style={{width: "max-content"}}
                                        className={"btn btn-info max-content"}>Next page
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default AdvancedJobSearch;