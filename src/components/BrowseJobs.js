import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../stylesheets/Navbar.css'
import '../stylesheets/Container.css'
import '../stylesheets/FiltersBar.css'
import '../stylesheets/ListItem.css'
//import {Link} from "react-router-dom";
import {
    getIndustries,
    getJobOffersBySkills,
    getJobOffersBySkill,
    getSkillsByIndustry,
    whoAmI,
    //getJobOffersByExample
} from "../repository/FetchData";
//import {forEach} from "react-bootstrap/cjs/utils/ElementChildren";
import JobCard from "./job-offers/JobCard";


class BrowseJobs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            PageNum: 0,
            Industries: [],
            Skills: [],
            Jobs: [],
            IsLoading: false
        };

        this.loadIndustries = this.loadIndustries.bind(this);
        this.loadSkillsByIndustry = this.loadSkillsByIndustry.bind(this);
        this.showCurrentUserDetails = this.showCurrentUserDetails.bind(this);
        this.searchBySkills = this.searchBySkills.bind(this);
        this.searchBySkill = this.searchBySkill.bind(this);
        this.linkButtons = this.linkButtons.bind(this);
        this.clearSearch = this.clearSearch.bind(this);
    }



    loadIndustries() {
        this.setState({
            IsLoading: true
        });

        getIndustries()
            .then(response => {
                this.setState({
                    Industries: response,
                    IsLoading: false
                });
                console.log("Loaded Industries:");
                console.log(this.state.Industries);
            });

    }


    loadSkillsByIndustry(id) {
        this.setState({
            IsLoading: true
        });

        getSkillsByIndustry(id)
            .then(response => {
                this.setState({
                    Skills: response,
                    IsLoading: false
                });
                console.log("Loaded Skills for industryId:" + id);
                console.log(this.state.Skills);
            });
    }

    componentDidMount() {
        this.loadIndustries();
        this.linkButtons();
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        this.linkButtons();
    }

    industrySelectionChanged() {
        console.log("industry selection changed, and the index is:");
        let select = document.getElementById("select");
        if (select != null) {
            console.log(select.selectedIndex);
            if (select.selectedIndex > 0) {
                this.loadSkillsByIndustry(select.selectedIndex);
                //this.loadSkillsByIndustry(this.state.Industries[select.selectedIndex]);
            }
        } else {
            console.log("null")
        }

    }

    showCurrentUserDetails() {
        whoAmI().then(response => {
            console.log(response);
        });
    }

    skillClick(id) {
        // let skill = document.getElementById(id);
        // let purple = document.getElementsByClassName("skill-item-purple");
        //
        // if (purple.length !== 0) {
        //     purple.item(0).setAttribute("class", "skill-item-white");
        // }
        //
        // skill.removeAttribute("class");
        // skill.setAttribute("class", "skill-item-purple");

        let skill = document.getElementById(id);
        if (skill.className === "skill-item-white") {
            skill.removeAttribute("class");
            skill.setAttribute("class", "skill-item-purple");
        } else {
            skill.removeAttribute("class");
            skill.setAttribute("class", "skill-item-white");
        }
    }

    searchBySkills() {
        let skills = document.getElementsByClassName("skill-item-purple");
        if (skills.length === 0)
            return;
        let skillIdsString = "";
        let skillIds = [];
        for (let i = 0; i < skills.length; i++) {
            if (i === skills.length - 1) {
                skillIdsString += skills[i].id;
                skillIds.push(skills[i].id)
                break;
            }
            skillIdsString += skills[i].id + ",";
            skillIds.push(skills[i].id)
        }
        const pageNum = this.state.PageNum;

        const params = new URLSearchParams({
            skillIds: skillIdsString,
            page: pageNum
        });
        console.log(params.toString());
        console.log(skillIds);
        console.log(params.get("skillIds"));
        //console.log("Requesting JobOffers by Skills:" + skillIdsString);
        // getJobOffersBySkills(skillIdsString, this.state.PageNum).then(response => {
        //     this.setState({
        //         Jobs: response,
        //         IsLoading: false
        //     })
        // })
        getJobOffersBySkills(params).then(response => {
            this.setState({
                Jobs: response,
                IsLoading: false
            })
        })

    }

    searchBySkill(pageNum) {
        if (document.getElementsByClassName("skill-item-purple").length !== 0)
            getJobOffersBySkill(document.getElementsByClassName("skill-item-purple")[0].id, pageNum)
                .then(response => {
                    console.log(response);
                    this.setState({
                        Jobs: response,
                        IsLoading: false
                    })
                })
    }

    // searchBySkill(pageNum) {
    //     if (document.getElementsByClassName("skill-item-purple").length !== 0)
    //         getJobOffersByExample(document.getElementsByClassName("skill-item-purple")[0].id, pageNum)
    //             .then(response => {
    //                 console.log(response);
    //                 this.setState({
    //                     Candidates: response,
    //                     IsLoading: false
    //                 })
    //             })
    // }

    linkButtons() {
        const btnPrev = document.getElementById("btnPrev");
        const btnNext = document.getElementById("btnNext");
        // console.log(btnPrev);
        // console.log(btnNext);
        if (btnPrev === null || btnPrev === undefined || btnNext === null || btnNext === undefined)
            return;

        btnPrev.setAttribute("style", "display:none");
        btnNext.setAttribute("style", "display:none");
        if (this.state.Jobs === undefined || this.state.Jobs.content === undefined || this.state.Jobs.length === 0) {
            return;
        }
        if (this.state.Jobs.first) {
            //console.log("first page");


        } else {
            btnPrev.removeAttribute("style");
            btnPrev.onclick = () => {
                this.setState({
                    PageNum: this.state.PageNum - 1
                })
                this.searchBySkills();
            }
        }
        if (this.state.Jobs.last) {

        } else {
            btnNext.removeAttribute("style");
            btnNext.onclick = () => {
                this.setState({
                    PageNum: this.state.PageNum + 1
                })
                this.searchBySkills();
            }
        }


    }

    clearSearch() {
        console.log("Clearing jobs");
        this.setState({Jobs: []})
    }

    render() {
        const industriesList = [];

        if (this.state.Industries === undefined) {
            industriesList.push(
                <h2> Just a sec... </h2>
            )
        } else {
            this.state.Industries.forEach(industry => {
                industriesList.push(<option
                    key={industry.id}
                    value={industry.id}
                >{industry.name}</option>)
            });
        }
        let skillsList = [];
        if (this.state.Skills !== undefined && this.state.Skills.length !== 0) {
            this.state.Skills.forEach(skill => {
                skillsList.push(
                    <li id={skill.id} key={skill.id} className={"skill-item-white"}
                        onClick={() => this.skillClick(skill.id)}>
                        {skill.name}
                    </li>
                )
            });
        }
        let jobsList = [];
        if (this.state.Jobs !== undefined && this.state.Jobs.length !== 0) {
            this.state.Jobs.content.forEach(job => {
                jobsList.push(<JobCard
                    key={job.id}
                    job={job}/>)
            });
        }
        else {
            jobsList = [];
        }

        return (
            <div className={"container"}>
                <div className="main-content">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <select id={"select"}
                                        className={"form-control"}
                                        onChange={() => this.industrySelectionChanged()}
                                    /* onFocus={()=>{this.selectedIndex = -1}}*/>
                                    <option key={-1} value={-1}>Select an industry</option>
                                    {industriesList}
                                </select>
                            </div>
                            <ul className="skills-list">
                                {skillsList}
                            </ul>
                        </div>
                        <div className="col-md-3">
                            <input id={"searchBtn"} type={"submit"} className={'btn btn-success btn-block'}
                                   onClick={() => this.searchBySkills()} value={"Search"}/>
                        </div>
                        <div className="col-md-3">
                            <input id={"clearBtn"} type={"submit"} className={'btn btn-success btn-block'}
                                   onClick={this.clearSearch} value={"Clear"}/>
                        </div>
                    </div>

                    <div>
                        <div className={"row"}>
                            {jobsList}
                        </div>
                        <div className={"row paging-buttons"}>
                            <div className={"col-md-10 col-lg-10"}>
                                <button id={"btnPrev"}
                                        className={"btn btn-info"}>Previous page
                                </button>
                            </div>

                            <div className={"col-md-2 col-lg-2"}>
                                <button id={"btnNext"}
                                    // style={{float: "right"}}
                                        className={"btn btn-info"}>Next page
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

export default BrowseJobs;