import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../stylesheets/Register.css'
import {Link} from "react-router-dom";
import {getUserById, login, register, whoAmI} from "../repository/FetchData";

//import {Link} from "react-router-dom";

class Register extends Component {

    submitHandler = event => {
        event.preventDefault();


        let userEmail = document.getElementById("email");
        if (userEmail === null) {
            window.alert("inputot za email e null");
            return;
        } else {
            if (this.emailValidator(userEmail.value)) {
                console.log("the email is ok")
            } else {
                window.alert("The email address is invalid");
                return;
            }
        }

        let userPassword = document.getElementById("password");
        if (userPassword === null) {
            window.alert("inputot za password e null");
            return;
        } else {
            if (userPassword.value.isEmpty || userPassword.value.length < 5) {
                window.alert("the password must be at least 5 characters");
                return;
            } else {
                console.log("The password is ok");
            }
        }

        const userType = document.getElementById("candidateRadio").checked ? "candidate" : "employer";
        console.log(`User type: ${userType}`);
        let userName = document.getElementById("name");
        if (userName === null) {
            window.alert("Please write your name");
            return;
        }

        let userPasswordRe = document.getElementById("re_password");
        if (userPasswordRe === null) {
            window.alert("inputot za repeat password e null");
            return;
        } else {
            if (userPasswordRe.value !== userPassword.value) {
                window.alert("the passwords dont match!");
                return;
            }
        }

        let user = {
            name: userName.value,
            email: userEmail.value,
            password: userPassword.value,
            roleId: 2
        };
        register(user, userType).then(response => {
            localStorage.removeItem("USER");
            //localStorage.setItem("USER", response.user);

            localStorage.removeItem("ACCESS_TOKEN");
            //localStorage.setItem("ACCESS_TOKEN", response.accessToken);
            console.log("SUCCESSFUL REGISTRATION");
            console.log(response);
            let loginUser = {
                email: user.email,
                password: user.password
            };
            login(loginUser).then(resp => {
                localStorage.setItem("ACCESS_TOKEN", resp.accessToken);
                localStorage.setItem("TOKEN_EXP", resp.expiresAt);
                console.log("SUCCESSFUL LOGIN");
                console.log(resp);
                // whoAmI().then(res => {
                //     console.log(res);
                //     localStorage.setItem("USER", JSON.stringify(res));
                //     window.location.replace("http://localhost:3000/");
                // });
                whoAmI().then(userPrincipalResponse => {
                    console.log(userPrincipalResponse);
                    localStorage.setItem("USER", JSON.stringify(userPrincipalResponse));
                    getUserById(userPrincipalResponse.id).then(fullUserResponse => {
                        console.log(fullUserResponse)
                        localStorage.setItem("FULL_USER", JSON.stringify(fullUserResponse));
                        if (fullUserResponse.hasOwnProperty("skills"))
                            localStorage.setItem("USER_TYPE", "CANDIDATE")
                        else if (fullUserResponse.hasOwnProperty("companyInfo"))
                            localStorage.setItem("USER_TYPE", "EMPLOYER")
                        window.location.replace("http://localhost:3000");
                    })
                });
            });

        }).catch(reason => console.log(reason));
    };

    emailValidator(email) {
        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }


    render() {
        return (
            <div className={'container'}>
                <link rel="stylesheet" href="../fonts/material-icon/css/material-design-iconic-font.min.css"/>
                <div className="row">
                    <div className="col-md-6 mx-auto">

                        <div className="signup-content">
                            <form onSubmit={this.submitHandler} id="signup-form" className="signup-form">
                                <h2 className="form-title">Create account</h2>

                                <div className="form-group">
                                    <input type="text" className="form-input" name="name" id="name"
                                           placeholder="Your Name"/>
                                </div>
                                <div className="form-group">
                                    <input type="email" className="form-input" name="email" id="email"
                                           placeholder="Your Email"/>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-input" name="password" id="password"
                                           placeholder="Password"/>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-input" name="re_password" id="re_password"
                                           placeholder="Repeat your password"/>
                                </div>

                                <div className={"form-group"}>
                                    <div className="form-check">
                                        <label className={"form-check-label"}>
                                            <input type="radio"
                                                   className="form-check-input"
                                                   name="radio"
                                                   id="candidateRadio"
                                                   defaultChecked={"checked"}
                                                   value={"candidate"}
                                            />
                                            <i>I am looking for a job.</i>
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <label className={"form-check-label"}>
                                            <input type="radio"
                                                   className="form-check-input"
                                                   name="radio"
                                                   id="employerRadio"
                                                   value={"employer"}
                                            />
                                            <i>I am an employer.</i>
                                        </label>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <input type="submit" name="submit" id="submit" className="form-submit"
                                           value="Sign up"/>
                                </div>
                            </form>
                            <p className="loginhere">
                                Already have an account? <Link to={"/login"}>Login here</Link>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Register;