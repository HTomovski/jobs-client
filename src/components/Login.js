import React, {Component} from "react";
import "bootstrap/dist/css/bootstrap.css";
import {Link} from "react-router-dom";
import {getUserById, login, whoAmI} from "../repository/FetchData"
//import '../stylesheets/Login.css'

class Login extends Component {

    submitHandler = event => {
        event.preventDefault();
        let userEmail = document.getElementById("email");
        if (userEmail === null){
            window.alert("inputot za email e null");
            return;
        }
        else {
            if (this.emailValidator(userEmail.value)){
                console.log("the email is ok")
            }
            else {
                window.alert("The email address is invalid");
                return;
            }
        }

        let userPassword = document.getElementById("password");
        if (userPassword === null) {
            window.alert("inputot za password e null");
            return;
        }

        else {
            if (userPassword.value.isEmpty || userPassword.value.length < 5){
                window.alert("the password must be at least 5 characters");
                return;
            }
            else {
                console.log("The password is ok");
            }
        }



        let user = {
            email: userEmail.value,
            password: userPassword.value
        };
        login(user).then(loginResponse => {
            localStorage.removeItem("USER");
            localStorage.removeItem("ACCESS_TOKEN");
            localStorage.removeItem("TOKEN_EXP");
            localStorage.setItem("ACCESS_TOKEN", loginResponse.accessToken);
            localStorage.setItem("TOKEN_EXP", loginResponse.expiresAt);
            console.log("SUCCESSFUL LOGIN");
            whoAmI().then(userPrincipalResponse => {
                console.log(userPrincipalResponse);
                localStorage.setItem("USER", JSON.stringify(userPrincipalResponse));
                getUserById(userPrincipalResponse.id).then(fullUserResponse => {
                    console.log(fullUserResponse)
                    localStorage.setItem("FULL_USER", JSON.stringify(fullUserResponse));
                    if (fullUserResponse.hasOwnProperty("skills"))
                        localStorage.setItem("USER_TYPE", "CANDIDATE")
                    else if (fullUserResponse.hasOwnProperty("companyInfo"))
                        localStorage.setItem("USER_TYPE", "EMPLOYER")
                    window.location.replace("http://localhost:3000");
                })
            });
        }).catch(reject => {
            alert("Incorrect credentials.");
        });
    };

    emailValidator(email) {
        //return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);

        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

    emailValidatorOnChange = event => {
        let isValid = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(event.target.value);
        if(isValid === true){
            console.log("'" + event.target.value.toString() + "' is a valid email address")
        }
        else {
            console.log("'" + event.target.value.toString() + "' is not a valid email address")
        }
    };

    render() {
        return (
            <div className={'container'}>
                <div className="row">
                    <div className="col-md-6 mx-auto">
                <div className="signup-content">
                    <form method="POST" id="signup-form" className="signup-form" onSubmit={this.submitHandler}>
                        <h2 className="form-title">Login</h2>
                        <div className="form-group">
                            <input type="email"
                                   className="form-input"
                                   name="email"
                                   id="email"
                                   placeholder="Your Email"
                                   //onChange={this.emailValidator}
                            />
                        </div>
                        <div className="form-group">
                            <input type="password"
                                   className="form-input"
                                   name="password"
                                   id="password"
                                   placeholder="Password"/>
                        </div>

                        <div className="form-group">
                            <input type="submit"
                                   name="submit"
                                   id="submit"
                                   className="form-submit" value="Login"/>
                        </div>
                    </form>
                    <p className="loginhere">
                        New here? <Link to={"/register"}>Register now</Link>
                    </p>
                </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Login;